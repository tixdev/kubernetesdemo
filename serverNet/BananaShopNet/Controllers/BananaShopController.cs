﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace BananaShopNet.Controllers
{
    [Route("[controller]")]
    public class BananaShopController : Controller
    {
        private readonly IBananaMetrics bananaMetrics;

        public BananaShopController(IBananaMetrics inMetrics)
        {
            this.bananaMetrics = inMetrics;
        }

        [HttpGet("/askForBanana")]
        public ActionResult AskForBanana()
        {
            bananaMetrics.addBanana();

            return Ok();
        }

        [HttpGet("/getTotalBananas")]
        public ActionResult GetTotalBananas()
        {
            return Ok(new
            {
                bananas = bananaMetrics.getBananas(),
                bananaPerMinute = bananaMetrics.getBananasPerMinute(),
                serverIp = GetLocalIPAddress(),
                serverName = GetHostName()
            });
        }

        [HttpGet("/health")]
        public ActionResult Health()
        {
            return Ok(new {IP = GetLocalIPAddress()});
        }

        private static string GetHostName()
        {
            return Dns.GetHostName();
        }

        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return "No IP was found";
        }
    }
}